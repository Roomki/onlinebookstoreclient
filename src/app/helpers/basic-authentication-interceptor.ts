
import{HttpInterceptor,HttpRequest,HttpHandler,HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';

export class BasicAuthenticationInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{

      let credOfUser = JSON.parse(localStorage.getItem('authUser'));

      if(credOfUser && credOfUser.authData){
          req = req.clone({
              setHeaders:{
                Authorization:`Basic ${credOfUser.authData}`
              }
          });
      }
       return next.handle(req);
    }

}