import { OnInit, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
    templateUrl:'./login.component.html'
})
export class LoginComponent implements OnInit{
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService) {}

        ngOnInit() {
            this.loginForm = this.formBuilder.group({
                userId: ['', Validators.required],
                password: ['', Validators.required]
            });    
            
            this.authenticationService.logout();    
            
            this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/bookStore';
        }

        get f() { return this.loginForm.controls; }

        onSubmit() {
            this.submitted = true;
    
            // stop here if form is invalid
            if (this.loginForm.invalid) {
                return;
            }
    
            this.loading = true;
            this.authenticationService.login(this.f.userId.value, this.f.password.value)                
                .subscribe(
                    data => {
                        this.router.navigate([this.returnUrl]);
                    },
                    error => {
                        this.error = error;
                        this.loading = false;
                    });
        }

}