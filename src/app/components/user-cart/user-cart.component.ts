import { OnInit, Component } from '@angular/core';
import { BookDetail } from '../../models/book-detail.model';
import { BookCartService } from '../../services/cart-service';

@Component({
    templateUrl:'./user-cart.component.html'
})
export class UserCartComponent implements OnInit{
    UserBookCartList: BookDetail[];
    constructor(private bookCartService: BookCartService ){

    }

    ngOnInit(){
this.bookCartService.currentCart.subscribe(bookSelectedList=>{
this.UserBookCartList = bookSelectedList;
});
}

}