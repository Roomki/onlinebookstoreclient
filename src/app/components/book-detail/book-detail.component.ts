import { Component, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';
import { BookDetail } from '../../models/book-detail.model';
import { BookCartService } from '../../services/cart-service';


@Component({
    templateUrl:'./book-detail.component.html'
})
export class BookDetailComponent implements OnInit{
    bookDetail: BookDetail[] = [];
    constructor(private bookService:BookService,
    private bookCartService: BookCartService){

    }
    ngOnInit(){
this.bookService.getAllBooks().subscribe(b=>{
    this.bookDetail = b;
});
    }

    AddToCart(id){
        var book = this.bookDetail.find(x=>x.BookId===id);
        this.bookCartService.AddIntoCart(book);
    }

}