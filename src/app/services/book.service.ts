import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import *  as Cons from '../constant';
import { BookDetail } from '../models/book-detail.model';

@Injectable({providedIn:'root'})
export class BookService{
    constructor(private http:HttpClient){

    }

    getAllBooks(){
        return this.http.get<BookDetail[]>(Cons.bookUrl,)
    }
}