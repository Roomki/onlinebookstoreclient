import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import *  as Cons from '../constant';
import { map } from 'rxjs/operators';

@Injectable({providedIn:'root'})
export class AuthenticationService{

    constructor(private http:HttpClient){}

    login(userId:string,password:string){        
        let url = Cons.loginUrl+"?userId="+userId+"&password="+password;    
    
    
        return this.http.post<any>(url,{userId,password})
        .pipe(map(user=>{
            if(user){
                user.authData = window.btoa(userId + ':' + password);
                localStorage.setItem('authUser', JSON.stringify(user));
            }
            return user;
        }));
    }

    logout() {        
        localStorage.removeItem('authUser');
    }
}

