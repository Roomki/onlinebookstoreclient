import { BehaviorSubject, Observable } from 'rxjs';
import { BookDetail } from '../models/book-detail.model';
import { Injectable } from '@angular/core';


@Injectable({providedIn:"root"})
export class BookCartService{

    private bookCartCurrent: BookDetail[];

    private bookCartUpdate = new BehaviorSubject<BookDetail[]>([]);

    currentCart = this.bookCartUpdate.asObservable();

   AddIntoCart(book: BookDetail){
       var currentCart = this.currentCart.subscribe(x=>{
           this.bookCartCurrent = x;
       });

       this.bookCartCurrent.push(book)
       
       this.bookCartUpdate.next(this.bookCartCurrent);
   }

}