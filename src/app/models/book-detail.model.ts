export class BookDetail{
    BookId:number;
    BookName:string;
    AuthorName:string;
    PublishedYear:string;
}