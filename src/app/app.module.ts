import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './guards/auth-guard';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BasicAuthenticationInterceptor } from './helpers/basic-authentication-interceptor';
import { ErrorInterceptor } from './helpers/error-interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { LoginComponent } from './components/login/login.component';
import { UserCartComponent } from './components/user-cart/user-cart.component';


@NgModule({
  declarations: [
    AppComponent,
    BookDetailComponent,
    LoginComponent,
    UserCartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
  {provide:HTTP_INTERCEPTORS,useClass:BasicAuthenticationInterceptor,multi:true},
  {provide:HTTP_INTERCEPTORS,useClass:ErrorInterceptor,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
